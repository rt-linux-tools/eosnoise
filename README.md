# eosnoise

In the context of high-performance computing (HPC), the Operating System
Noise (osnoise) refers to the interference experienced by an application
due to activities inside the operating system. In the context of Linux,
NMIs, IRQs, SoftIRQs, and any other system thread can cause noise to the
system. Moreover, hardware-related jobs can also cause noise, for example,
via SMIs.

the osnoise tool loops reading the time, looking for discontinuity that
might point to an source of osnoise.

This tool is augmented by eBPF, enabling the collection of some kernel
statistics, such as the number of threads, IRQs, SoftIRQs and NMIs that
potentially created noise to the tool. It also collects trace of the
execution of these kind of tasks, helping the user to identify the cause
of a single noise.

This tool was based on the ftrace/kernel implementation and is still in
a PoC phase.

## Command Line Options

	osnoise: measures the operating system noise, augmented by eBPF
	usage: osnoise [-n] [-v] [-h] \
		[-c cpu-list] \
		[-r time in us] [-p time in us] \
		[-s time in us ] [ -S time in us ] \
		[-t time in ns ]

	       options:
		   -n/--no_trace: do not dump the trace buffer
		   -v/--verbose: verbose output
		   -h/--help: print this message
		   -r/--runtime: runtime in us
		   -p/--period: period in us
		   -t/--tolerance: minimum gap to be considered a noise
		   -s/--stop_single_thresh: stop the tool if a single noise > thresh is found
		   -S/--stop_total_thresh: stop the tool if a total noise > thresh is found

## Installing

On Fedora, the user needs to install:

	sudo dnf install glibc-devel glibc-devel.i686 bpftool

### setup libbpf git sub-module

After cloning the eosnoise git repository, change directory into
where you cloned it, then issue the following commands to setup the
libbpf submodule:

	git submodule update --init libbpf

## Building

	make
	make install

## Author
Daniel Bristot de Oliveira <bristot@kernel.org>
