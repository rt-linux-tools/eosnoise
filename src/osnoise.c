/* SPDX-License-Identifier: GPL v2 */
/*
 * Copyright (c) 2021 Daniel Bristot de Oliveira <bristot@redhat.com>
 */
#define _GNU_SOURCE

#include <argp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <bpf/libbpf.h>
#include <bpf/bpf.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/syscall.h>
#include "osnoise.h"
#include "osnoise.skel.h"
#include <sys/time.h>
#include <sys/resource.h>

#include <pthread.h>

struct cpu_info {
	int cpu;
	int sleep_time;
	int pid;
	pthread_t thread;
};

static volatile bool exiting = 0;

struct osnoise_bpf *obj;
struct perf_buffer *pb = NULL;

#define NSEC_PER_SEC		1000000000ULL
#define NSEC_PER_MSEC		1000000ULL
#define NSEC_PER_USEC		1000ULL
#define USEC_PER_SEC		1000000ULL

#define SEC_TO_USEC(s)		((s) * USEC_PER_SEC)
#define SEC_TO_NSEC(s)		((s) * NSEC_PER_SEC)
#define MSEC_TO_NSEC(s)		((s) * NSEC_PER_MSEC)
#define USEC_TO_NSEC(u)		((u) * 1000)
#define USEC_TO_SEC(u)		((u) / USEC_PER_SEC)
#define NSEC_TO_SEC(u)		((u) / NSEC_PER_SEC)
#define NSEC_TO_USEC(n)		((n) / 1000)
#define NSEC_NO_SEC(n)		((n) % NSEC_PER_SEC)


char *config_cpus;
char config_all_cpus = 1;
int config_verbose = 0;
int config_nr_cpus = 0;
int config_runtime_ns = MSEC_TO_NSEC(950);
int config_period_ns = MSEC_TO_NSEC(1000);
int config_tolerance_ns = 5000;
int config_trace_single_thresh = 0;
int config_trace_total_thresh = 0;
int config_do_not_trace = 0;

/**
 * interface lock.
 */
pthread_mutex_t interface_lock;

/*
 * print any error messages and exit
 */
void die(const char *fmt, ...)
{
	volatile int zero = 0;
	va_list ap;
	int ret = errno;

	if (errno)
		perror("osnoise: ");
	else
		ret = -1;

	va_start(ap, fmt);
	fprintf(stderr, "  ");
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fprintf(stderr, "\n");

	/*
	 * Die with a divizion by zero to keep the stack on GDB.
	 */
	if (config_verbose)
		zero = 10 / zero;

	exit(ret);
}

/**
 * libbpf_print_fn - libbpf print callback
 */
int libbpf_print_fn(enum libbpf_print_level level,
		const char *format, va_list args)
{
	if (level == LIBBPF_DEBUG)
		return 0;

	if (!config_verbose)
		return 0;

	return vfprintf(stderr, format, args);
}

/*
 * time_get - get time in u64
 */
unsigned long long time_get(void)
{
	struct timespec ts;

	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ts.tv_sec * NSEC_PER_SEC + ts.tv_nsec;
}

/**
 * bump_memlock_rlimit - increase the memlock limit
 *
 * Required for eBPF.
 */
int bump_memlock_rlimit(void)
{
	struct rlimit rlim_new = {
		.rlim_cur	= RLIM_INFINITY,
		.rlim_max	= RLIM_INFINITY,
	};

	return setrlimit(RLIMIT_MEMLOCK, &rlim_new);
}

/**
 * sig_handler - capture ^c to stop the tool
 */
static void sig_handler(int sig)
{
	exiting = true;
}

/**
 * handle_event - callback function to parse perf buffer
 *
 * This function is called to parse each event posted in
 * the perf buffer via "post_event" in the bpf code.
 */
void handle_event(void *ctx, int cpu, void *data, __u32 data_sz)
{
	const struct osnoise_event *e = data;
	const struct osnoise_switch_event *s = data;

	switch (e->event) {
	case EVENT_OSNOISE_NMI:
		printf("%llu.%09llu     nmi:        duration: %llu ns\n",
				NSEC_TO_SEC(e->start_time),
				NSEC_NO_SEC(e->start_time),
				e->duration);
		break;
	case EVENT_OSNOISE_IRQ:
		printf("%llu.%09llu     irq: %16s:%-6d duration: %llu ns\n",
				NSEC_TO_SEC(e->start_time),
				NSEC_NO_SEC(e->start_time),
				e->name,
				e->id,
				e->duration);
		break;
	case EVENT_OSNOISE_SOFTIRQ:
		printf("%llu.%09llu softirq:             sirq:%-6d duration: %llu ns\n",
				NSEC_TO_SEC(e->start_time),
				NSEC_NO_SEC(e->start_time),
				e->id,
				e->duration);
		break;
	case EVENT_OSNOISE_THREAD:
		printf("%llu.%09llu  thread: %16s:%-6d duration: %llu ns\n",
				NSEC_TO_SEC(e->start_time),
				NSEC_NO_SEC(e->start_time),
				e->name,
				e->id,
				e->duration);
		break;
	case EVENT_SWITCH_IN:
		printf("%llu.%09llu switch in -----> \n",
				NSEC_TO_SEC(s->time),
				NSEC_NO_SEC(s->time));
		break;
	case EVENT_SWITCH_OUT:
		printf("%llu.%09llu switch out <----- status: %d\n",
				NSEC_TO_SEC(s->time),
				NSEC_NO_SEC(s->time),
				s->status);
		break;
	}
}

/**
 * handle_lost_events - print info about lost events on perf buffer
 */
void handle_lost_events(void *ctx, int cpu, __u64 lost_cnt)
{
	printf("trace lost %llu events on CPU #%d!\n", lost_cnt, cpu);
}

/**
 * perf_print_cpu_buffer - dump the per-cpu perf buffer
 */
void perf_print_cpu_buffer(int cpu)
{
	int err;
	err = perf_buffer__consume_buffer(pb, cpu);
	if (err)
		printf("Error reading perf buffer: %d\n", err);
}

/**
 * load_ebpf_context - sets up ebpf context
 *
 * Set up the basics for the ebpf program to run, raising
 * memlock limit, loading and attaching the eBPF code, set
 * up the perf buffer and return the ebpf object.
 */
struct osnoise_bpf *load_ebpf_context(void)
{
	struct perf_buffer_opts pb_opts;

	struct osnoise_bpf *obj;
	int err;

	libbpf_set_print(libbpf_print_fn);

	err = bump_memlock_rlimit();
	if (err) {
		fprintf(stderr, "failed to increase rlimit: %d\n", err);
		return 0;
	}

	obj = osnoise_bpf__open();
	if (!obj) {
		fprintf(stderr, "failed to open and/or load BPF object\n");
		return 0;
	}

	err = osnoise_bpf__load(obj);
	if (err) {
		fprintf(stderr, "failed to load BPF object: %d\n", err);
		goto cleanup;
	}

	err = osnoise_bpf__attach(obj);
	if (err) {
		fprintf(stderr, "failed to attach BPF programs\n");
		goto cleanup;
	}

	pb_opts.sample_cb = handle_event;
	pb_opts.lost_cb = handle_lost_events;

	pb = perf_buffer__new(bpf_map__fd(obj->maps.events), 16384, &pb_opts);
	err = libbpf_get_error(pb);

	if (err) {
		pb = NULL;
		fprintf(stderr, "failed to open perf buffer: %d\n", err);
		goto cleanup;
	}

	return obj;

cleanup:
	osnoise_bpf__destroy(obj);
	return 0;
}

/**
 * get_osn_per_cpu_vars - get the content of the per_cpu_osn_vars maps
 *
 * The osnoise tracer saves the counters in a per-cpu map, this function
 * reads the content of this map.
 *
 * I wonder if it is possible to read the content of each CPU independently,
 * like in kernel. There is for sure room for improvement here.
 */
static int get_osn_per_cpu_vars(struct osnoise_variables *osn)
{
	struct bpf_map *map = obj->maps.per_cpu_osn_vars;
	int fd = bpf_map__fd(map);
	__u32 key = 0;

	if (bpf_map_lookup_elem(fd, &key, osn) != 0) {
		fprintf(stderr, "failed to lookup osn per cpu vars\n");
		return -ENODATA;
	}

	return 0;
}

/**
 * set_osn_per_cpu_vars - update the contente of the per_cpu_osn_vars maps
 *
 * Set the content of the eBPF map with the content from user-space.
 */
static int set_osn_per_cpu_vars(struct osnoise_variables *osn)
{
	struct bpf_map *map = obj->maps.per_cpu_osn_vars;
	int fd = bpf_map__fd(map);
	__u32 key = 0;

	if (bpf_map_update_elem(fd, &key, osn, 0) < 0) {
		 fprintf(stderr, "failed to update osn per cpu vars\n");
		 return -EINVAL;
	}

	return 0;
}

/**
 * stop_sampling - Let the eBPF side known it should stop tracing
 */
static void stop_sampling(void)
{
	struct osnoise_variables curr[config_nr_cpus];
	int i;

	get_osn_per_cpu_vars(curr);

	for (i = 0; i < config_nr_cpus; i++)
		curr[i].sampling = 0;

	set_osn_per_cpu_vars(curr);

}

/**
 * init_per_cpu_vars - init a user-space struct with values from kernel
 *
 * This function gets the kernel map and updates the value of the
 * osn structure with the data of the given CPU.
 */
static int
init_per_cpu_vars(struct osnoise_variables *osn, int cpu)
{
	struct osnoise_variables curr[config_nr_cpus];
	int retval;

	retval = get_osn_per_cpu_vars(curr);
	if (retval)
		return -ENODATA;

	 osn->nmi.count = curr[cpu].nmi.count;
	 osn->irq.count = curr[cpu].irq.count;
	 osn->softirq.count = curr[cpu].softirq.count;
	 osn->thread.count = curr[cpu].thread.count;

	 return 0;
}

/**
 * get_diff_per_cpu_vars - update and diff osn variables
 *
 * This function receives two osn variables: one with a previous read of
 * the variables, and one to store the diff values. This function will
 * read the osn variables from kernel and compute the previous one's
 * delta, saving the results into the diff variables. Then the previous
 * structure will receive the values of the new read for later comparison.
 */
static int
get_diff_per_cpu_vars(struct osnoise_variables *prev,
		      struct osnoise_variables *diff,
		      unsigned int cpu)
{
	struct osnoise_variables curr[config_nr_cpus];
	int retval;

	retval = get_osn_per_cpu_vars(curr);
	if (retval)
		return -ENODATA;

	diff->pid = curr[cpu].pid;
	prev->pid = curr[cpu].pid;

	diff->nmi.count = curr[cpu].nmi.count - prev->nmi.count;
	prev->nmi.count = curr[cpu].nmi.count;

	diff->irq.count = curr[cpu].irq.count - prev->irq.count;
	prev->irq.count = curr[cpu].irq.count;

	diff->softirq.count = curr[cpu].softirq.count - prev->softirq.count;
	prev->softirq.count = curr[cpu].softirq.count;

	diff->thread.count = curr[cpu].thread.count - prev->thread.count;
	prev->thread.count = curr[cpu].thread.count;

	return 0;
}

/**
 * print_osnoise_header - Print a header with information about the output
 */
void print_osnoise_header(void)
{
	printf("                                         %% of CPU    MAX SINGLE\n");
	printf("CPU        RUNTIME      TOTAL NOISE     AVAILABLE         NOISE       NMI       IRQ      SIRQ     Thread\n");

}

/**
 * print_osnoise_vars - print the values of an osn variable
 */
static void
print_osnoise_vars(struct osnoise_variables *osn)
{

	printf(" %9llu", osn->nmi.count);
	printf(" %9llu", osn->irq.count);
	printf(" %9llu", osn->softirq.count);
	printf(" %9llu", osn->thread.count);
	return;
}

/**
 * print_diff_osnoise_vars - Compute the diff and print the stats
 *
 * Compute the diff of the osn struct with current values, save
 * print the diff and save the current values in the osn. This
 * for the CPU passed as argument.
 */
static int
print_diff_osnoise_vars(struct osnoise_variables *osn, unsigned int cpu)
{
	struct osnoise_variables diff[config_nr_cpus];
	int retval;

	retval = get_diff_per_cpu_vars(osn, diff, cpu);
	if (retval)
		return retval;

	print_osnoise_vars(diff);

	return 0;
}

/**
 * print_cpu_noise_ratio - compute the % of CPU available for the tool
 */
void print_cpu_noise_ratio(unsigned long long runtime, unsigned long long noise)
{
	unsigned long long net_runtime = runtime - noise;
	unsigned long long ratio = net_runtime * 10000000;
	unsigned long long ratio_dec;

	ratio = ratio / runtime;
	ratio_dec = ratio % 100000;
	ratio = ratio / 100000;

	printf("    %3llu.%05llu %%", ratio, ratio_dec);

	return;
}

/**
 * osnoise_main - sampling thread main
 *
 * This is the coreo of osnoise. This function is the main of a sampling
 * thread. Firstly, it sets its affinity to the CPU it will sample.
 *
 * The main while loop resets the variables, reads the osn variables, and
 * then starts sampling the noise by reading the time in the do {} while
 * loop.
 *
 * If a single noise higher than the config_trace_single_thesh is found,
 * it dumps the trace immediately. If a config_trace_total_thesh is found
 * at the end of the loop, it dumps the trace after the periodic report.
 */
void *osnoise_main(void *data)
{
	struct cpu_info *cpu_info = (struct cpu_info *) data;
	unsigned long long noise, sum_noise, max_noise;
	unsigned long long sample, last_sample;
	struct osnoise_variables osn_vars;
	int cpu = cpu_info->cpu;
	unsigned int sleep_time;
	signed long long total;
	pthread_t thread;
	cpu_set_t mask;
	int retval;

	sleep_time = NSEC_TO_USEC(config_period_ns - config_runtime_ns);

	cpu_info->pid = syscall(__NR_gettid);

	CPU_ZERO(&mask);
	CPU_SET(cpu, &mask);
	thread = pthread_self();

	if (pthread_setaffinity_np(thread, sizeof(mask), &mask) != 0)
		printf("Could not set CPU affinity to CPU #%d\n", cpu_info->cpu);

	/*
	 * Wait for the things to settle down.
	 */
	sleep(1);

	while (!exiting) {

		noise = 0;
		sum_noise = 0;
		max_noise = 0;
		total = 0;

		retval = init_per_cpu_vars(&osn_vars, cpu);
		if (retval)
			die("error getting osn variables from kernel\n");

		last_sample = time_get();
		do {
			sample = time_get();
			noise = sample - last_sample;
			total += noise;

			if (noise > config_tolerance_ns) {
				if (config_verbose)
					printf("CPU %d: noise %llu\n", cpu, noise);

				sum_noise += noise;

				if (noise > max_noise)
					max_noise = noise;

				if (config_trace_single_thresh &&
				    (config_trace_single_thresh <= noise)) {

					pthread_mutex_lock(&interface_lock);
					stop_sampling();
					printf("CPU:%d hit a single noise of %llu\n", cpu, NSEC_TO_USEC(noise));

					if (!config_do_not_trace) {
						printf("dumping trace\n");
						perf_print_cpu_buffer(cpu);
					}
					pthread_mutex_unlock(&interface_lock);

					exit(1);
				}
			}

			last_sample  = sample;
		} while (total < config_runtime_ns);

		pthread_mutex_lock(&interface_lock);
		printf("[%03u]", cpu);
		printf(" %12llu", NSEC_TO_USEC(total));
		printf(" %16llu", NSEC_TO_USEC(sum_noise));
		print_cpu_noise_ratio(total, sum_noise);
		printf(" %12llu", NSEC_TO_USEC(max_noise));
		print_diff_osnoise_vars(&osn_vars, cpu);
		printf("\n");

		if (config_trace_total_thresh && (config_trace_total_thresh <= sum_noise)) {
			stop_sampling();
			printf("CPU:%d hit a total noise of %llu\n", cpu, NSEC_TO_USEC(sum_noise));
			if (!config_do_not_trace) {
				printf("dumping trace\n");
				perf_print_cpu_buffer(cpu);
			}
			exit(1);
		}
		pthread_mutex_unlock(&interface_lock);

		if (sleep_time)
			usleep(sleep_time);
	}

	return 0;
}

/**
 * create_threads - create sampling threads
 */
void create_threads(struct cpu_info *cpus)
{
	int i;

	for (i = 0; i < config_nr_cpus; i++) {
		if (!config_all_cpus && !config_cpus[i])
			continue;

		cpus[i].cpu = i;
		pthread_create(&cpus[i].thread, NULL, osnoise_main, &cpus[i]);
	}
}

/**
 * parse_cpu_list - parse a cpu list string, saving it into config
 */
static void parse_cpu_list(char *cpulist)
{
	const char *p;
	int end_cpu;
	int cpu;
	int i;

	config_cpus = malloc(config_nr_cpus * sizeof(char));
	memset(config_cpus, 0, (config_nr_cpus * sizeof(char)));

	for (p = cpulist; *p; ) {
		cpu = atoi(p);
		if (cpu < 0 || (!cpu && *p != '0') || cpu > config_nr_cpus)
			goto err;

		while (isdigit(*p))
			p++;
		if (*p == '-') {
			p++;
			end_cpu = atoi(p);
			if (end_cpu < cpu || (!end_cpu && *p != '0'))
				goto err;
			while (isdigit(*p))
				p++;
		} else
			end_cpu = cpu;

		if (cpu == end_cpu) {
			if (config_verbose)
				printf("cpulist: adding cpu %d\n", cpu);
			config_cpus[cpu] = 1;
		} else {
			for (i = cpu; i <= end_cpu; i++) {
				if (config_verbose)
					printf("cpulist: adding cpu %d\n", i);
				config_cpus[i] = 1;
			}
		}

		if (*p == ',')
			p++;
	}

	return;
err:
	die("Error parsing the cpu list %s", cpulist);
}

/**
 * get_long_from_str - Parse a str converting to long
 */
long get_long_from_str(char *start)
{
	long value;
	char *end;

	errno = 0;
	value = strtol(start, &end, 10);
	if (errno || start == end)
		return -1;

	return value;
}

static void print_usage(char *err)
{
	int i;

	if (err)
		fprintf(stderr, "%s\n", err);

	char *msg[] = {
		"osnoise: measures the operating system noise, augmented by eBPF",
		"  usage: osnoise [-n] [-v] [-h] \\",
		"          [-c cpu-list] \\",
		"          [-r time in us] [-p time in us] \\",
		"          [-s time in us ] [ -S time in us ] \\",
		"          [-t time in ns ]",
		"",
		"       options:",
		"	   -n/--no_trace: do not dump the trace buffer",
		"	   -v/--verbose: verbose output",
		"	   -h/--help: print this message",
		"	   -r/--runtime: runtime in us",
		"	   -p/--period: period in us",
		"	   -t/--tolerance: minimum gap to be considered a noise",
		"	   -s/--stop_single_thresh: stop the tool if a single noise > thresh is found",
		"	   -S/--stop_total_thresh: stop the tool if a total noise > thresh is found",
		NULL,
	};

	for(i = 0; msg[i]; i++)
		fprintf(stderr, "%s\n", msg[i]);
	exit(0);
}

/**
 * parse_args - parse command line arguments
 *
 * Timing arguments are in us for the user, but they are stored in ns
 * internally because the time_get() is in ns.
 */
static int parse_args(int argc, char **argv)
{
	int c;

	while (1) {
		static struct option long_options[] = {
			{"cpu",		 	required_argument,	0, 'c'},
			{"verbose",		no_argument, 		0, 'v'},
			{"no_trace",		no_argument, 		0, 'n'},
			{"help",		no_argument, 		0, 'h'},
			{"runtime",		required_argument,	0, 'r'},
			{"period",		required_argument,	0, 'p'},
			{"stop_single_thresh",	required_argument,	0, 's'},
			{"stop_total_thresh",	required_argument,	0, 'S'},
			{"tolerance",		required_argument,	0, 't'},
			{0, 0, 0, 0}
		};

		/* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long(argc, argv, "c:vnhr:p:s:S:t:",
				 long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c) {
		case 'c':
			config_all_cpus = 0;
			parse_cpu_list(optarg);
			break;
		case 'v':
			config_verbose = 1;
			break;
		case 'r':
			config_runtime_ns = get_long_from_str(optarg);
			if (config_runtime_ns == -1)
				die("wrong runtime_us value\n");
			config_runtime_ns = USEC_TO_NSEC(config_runtime_ns);
		case 'p':
			config_period_ns = get_long_from_str(optarg);
			if (config_period_ns == -1)
				die("wrong period_us value\n");
			config_period_ns = USEC_TO_NSEC(config_period_ns);
		case 's':
			config_trace_single_thresh = get_long_from_str(optarg);
			if (config_trace_single_thresh == -1)
				die("wrong trace_single_thresh value\n");
			config_trace_single_thresh = USEC_TO_NSEC(config_trace_single_thresh);
			break;
		case 'S':
			config_trace_total_thresh = get_long_from_str(optarg);
			if (config_trace_total_thresh == -1)
				die("wrong trace_total_thresh value\n");
			config_trace_total_thresh = USEC_TO_NSEC(config_trace_total_thresh);
			break;
		case 't':
			config_tolerance_ns = get_long_from_str(optarg);
			if (config_tolerance_ns == -1)
				die("wrong tolerance_ns value\n");
			break;
		case 'n':
			config_do_not_trace = true;
			break;
		case 'h':
		default:
			print_usage(NULL);
		}

		if (config_runtime_ns > config_period_ns)
			print_usage("runtime should be smaller than the period\n");

		if (config_trace_single_thresh >= config_runtime_ns)
			print_usage("trace single thresh should be smaller than the runtime\n");

		if (config_trace_total_thresh >= config_runtime_ns)
			print_usage("trace total thresh should be smaller than the runtime\n");

		if (config_tolerance_ns >= config_runtime_ns)
			print_usage("tolerance should be smaller than the runtime\n");

	}

	return(0);
}

/**
 * main - main
 *
 * Parse the arguments, load the eBPF, create the threads, init data
 * and wait...
 */
int main(int argc, char **argv)
{
	unsigned long nr_cpus = libbpf_num_possible_cpus();
	struct osnoise_variables osn_vars[nr_cpus];
	struct cpu_info cpus[nr_cpus];
	int i;

	config_nr_cpus = nr_cpus;
	parse_args(argc, argv);

	if (pthread_mutex_init(&interface_lock, NULL) != 0)
		die("fail to init the interface mutex\n");

	obj = load_ebpf_context();
	if (!obj)
		die("error loading eBPF\n");

	signal(SIGINT, sig_handler);

	create_threads(cpus);

	/* Init the data */
	get_osn_per_cpu_vars(osn_vars);

	for (i = 0; i < nr_cpus; i++) {

		if (!config_all_cpus && !config_cpus[i])
			continue;

		while (!cpus[i].pid);
		osn_vars[i].sampling = 1;
		osn_vars[i].pid = cpus[i].pid;
	}

	set_osn_per_cpu_vars(osn_vars);
	print_osnoise_header();
	/* main: poll */
	while (1) {
		sleep(10000000);
		if (exiting)
			break;
	}

	perf_buffer__free(pb);
	osnoise_bpf__destroy(obj);

	return 0;
}
