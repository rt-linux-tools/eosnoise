/* SPDX-License-Identifier: (LGPL-2.1 OR BSD-2-Clause) */
#ifndef __OSNOISE_H
#define __OSNOISE_H

#define MAX_SLOTS	20

struct irq_key {
	char name[32];
};

struct info {
	__u64 count;
	__u64 max_duration;
};

/*
 * NMI runtime info.
 */
struct nmi {
	__u64 count;
	__u64 delta_start;
};

/*
 * IRQ runtime info.
 */
struct irq {
	__u64 count;
	__u64 arrival_time;
	__u64 delta_start;
};

/*
 * SofIRQ runtime info.
 */
struct softirq {
	__u64 count;
	__u64 arrival_time;
	__u64 delta_start;
};

/*
 * Thread runtime info.
 */
struct thread {
	__u64 count;
	__u64 arrival_time;
	__u64 delta_start;
};

/*
 * Runtime information: this structure saves the runtime information used by
 * one sampling thread.
 */
struct osnoise_variables {
	bool sampling;
	int pid;
	struct nmi nmi;
	struct irq irq;
	struct softirq softirq;
	struct thread thread;
	int int_counter;
};

#define TASK_NAME_LEN	16
enum event_set {
        EVENT_OSNOISE_NMI = 0,
        EVENT_OSNOISE_IRQ,
       	EVENT_OSNOISE_SOFTIRQ,
       	EVENT_OSNOISE_THREAD,
	EVENT_SWITCH_IN,
	EVENT_SWITCH_OUT,
};

struct osnoise_event {
	int event;
	int id;
	__u64 start_time;
	__u64 duration;
	char name[TASK_NAME_LEN];
};

struct osnoise_switch_event {
	int event;
	__u64 time;
	__u32 status;
	__u32 preempt;
};

#endif /* __OSNOISE_H */
