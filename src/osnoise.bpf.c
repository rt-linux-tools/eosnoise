// SPDX-License-Identifier: GPL-2.0
/*
 * Based on the osnoise tracer.
 *
 * Copyright (c) 2021 Daniel Bristot de Oliveira <bristot@redhat.com>
 */

#include "vmlinux.h"
#include <string.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_tracing.h>
#include "osnoise.h"

/**
 * per_cpu_osn_vars - per-cpu statistics
 */
struct {
	__uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
	__uint(max_entries, 1);
	__type(key, u32);
	__type(value, struct osnoise_variables);
} per_cpu_osn_vars SEC(".maps");

/**
 * events - perf buffer to send events to user-space
 */
struct {
	__uint(type, BPF_MAP_TYPE_PERF_EVENT_ARRAY);
	__uint(key_size, sizeof(u32));
	__uint(value_size, sizeof(u32));
} events SEC(".maps");

static struct info zero;

#define time_get	bpf_ktime_get_ns
#define BPF_F_CURRENT_CPU 0xffffffffULL

/**
 * get_this_cpu_osn_vars - Get the cpu´s  osnoise_variables
 *
 * Each CPU has its own set of statistics stored on a per-cpu
 * array, this function returns the variable of the current
 * CPU.
 */
static struct osnoise_variables *get_this_cpu_osn_vars(void)
{
	struct osnoise_variables *osn_var;
	u32 key = 0;

	osn_var = bpf_map_lookup_elem(&per_cpu_osn_vars, &key);

	if (!osn_var) {
		bpf_printk("error getting this cpu var");
		return 0;
	}

	return osn_var;
}

/**
 * post_event - Post an event to the BPF buffer
 *
 * This is the tracer part of the tool. This function posts an
 * event in the perf buffer, that will be read in user-space.
 */
static int
post_event(void *ctx, enum event_set event_id, const char *name, int id,
	   u64 start_time, u64 duration)
{

	struct osnoise_event event = {
		.event = event_id,
		.id = id,
		.start_time = start_time,
		.duration = duration
	};

	__builtin_memcpy(event.name, name, 16);

	bpf_printk("name %s", name);

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU, &event,
			      sizeof(event));

	return 0;
}

/**
 * post_switch_event - post sched switch event of the sampling thread
 */
static int
post_switch_event(void *ctx, bool in, int preempt, int status)
{
	struct osnoise_switch_event event;

	memset(&event, 0, sizeof(event));

	if (in)
		event.event = EVENT_SWITCH_IN;
	else
		event.event = EVENT_SWITCH_OUT;

	event.time = time_get();
	event.preempt = preempt;
	event.status = status;

	bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU, &event,
			      sizeof(event));

	return 0;
}

/*
 * The next functions are the hookers to the trace events. It
 * is sorted by priority: NMI, IRQ, SoftIRQ and Thread.
 */

/**
 * handle__nmi_handler - handle the single NMI event
 *
 * NMI has only one event that already print the duration. So
 * a single event is enough.
 */
const char nmi_name[16] = "NMI";
SEC("tp_btf/nmi_handler")
int handle__nmi_handler(u64 *ctx)
{
	struct osnoise_variables *osn_var = get_this_cpu_osn_vars();
	s64 delta_ns = ctx[1];
	u64 start;

	start = time_get() - delta_ns;

	if (!osn_var)
		return 0;

	if (!osn_var->sampling)
		return 0;

	osn_var->int_counter++;
	osn_var->nmi.count++;

	post_event(ctx, EVENT_OSNOISE_NMI, nmi_name, 0, start, delta_ns);

	return 0;
}

/**
 * handle_irq_entry - Generic function for taking note of hard irq entry
 *
 * This function is used by the functions that hook into the _entry
 * trace event of the IRQ handlers.
 */
static void handle_irq_entry(void)
{
	struct osnoise_variables *osn_var = get_this_cpu_osn_vars();

	if (!osn_var)
		return;

	if (!osn_var->sampling)
		return;

	osn_var->irq.count++;
	osn_var->irq.arrival_time = time_get();
	osn_var->int_counter++;
}

/**
 * handle_irq_exit - Generic function for taking note of hard irq exit
 *
 * This function is used by the functions that hook into the _entry
 * trace event of the IRQ handlers.
 */
static void handle_irq_exit(void *ctx, int irq, const char *desc)
{
	struct osnoise_variables *osn_var = get_this_cpu_osn_vars();
	s64 delta;
	char name[16];
	int i;

	if (!osn_var)
		return;

	if (!osn_var->sampling)
		return;

	delta = time_get() - osn_var->irq.arrival_time;

	post_event(ctx, EVENT_OSNOISE_IRQ, desc, irq,
		   osn_var->irq.arrival_time, delta);
}

/*
 * The next functions are hookers for hard irqs, they
 * all do the same thing: hook to the entry and exit
 * trace events, and call the generic functions.
 */
SEC("tp_btf/irq_handler_entry")
int handle__irq_handler_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

SEC("tp_btf/irq_handler_exit")
int handle__irq_handler_exit(u64 *ctx)
{
	struct irqaction *action = (void *) ctx[1];
	char name[16] = "device";
	int irq = ctx[0];

	handle_irq_exit(ctx, irq, name);

	return 0;
}

SEC("tp_btf/local_timer_entry")
int handle__local_timer_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}


const char local_timer[16] = "local_timer";
SEC("tp_btf/local_timer_exit")
int handle__local_timer_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, local_timer);
	return 0;
}

SEC("tp_btf/spurious_apic_entry")
int handle__spurious_apic_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

const char spurious_apic[16] = "spurious_apic";
SEC("tp_btf/spurious_apic_exit")
int handle__spurious_apic_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, spurious_apic);
	return 0;
}


/*
 *
 * Like on perf, the irq_work interrupt cannot be traced
 * using eBPF.
 */
#if 0
SEC("tp_btf/irq_work_entry")
int handle__irq_work_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

const char *irq_work = "irq_work";
SEC("tp_btf/irq_work_exit")
int handle__irq_work_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, irq_work);
	return 0;
}
#endif

SEC("tp_btf/reschedule_entry")
int handle__reschedule_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

const char reschedule[16] = "reschedule";
SEC("tp_btf/reschedule_exit")
int handle__reschedule_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, reschedule);
	return 0;
}

SEC("tp_btf/call_function_single_entry")
int handle__call_function_single_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

char call_function_single[16] = "function_single";
SEC("tp_btf/call_function_single_exit")
int handle__call_function_single_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, call_function_single);
	return 0;
}

SEC("tp_btf/call_function_entry")
int handle__call_function_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

const char call_function[16] = "function";
SEC("tp_btf/call_function_exit")
int handle__call_function_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, call_function);
	return 0;
}

SEC("tp_btf/thermal_apic_entry")
int handle__thermal_apic_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

const char thermal_apic[16] = "thermal_apic";
SEC("tp_btf/thermal_apic_exit")
int handle__thermal_apic_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, thermal_apic);
	return 0;
}

SEC("tp_btf/deferred_error_apic_entry")
int handle__deferred_error_apic_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

const char deferred_error_apic[16] = "def_error_apic";
SEC("tp_btf/deferred_error_apic_exit")
int handle__deferred_error_apic_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, deferred_error_apic);
	return 0;
}

SEC("tp_btf/threshold_apic_entry")
int handle__threshold_apic_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

const char threshold_apic[16] = "threshold_apic";
SEC("tp_btf/threshold_apic_exit")
int handle__threshold_apic_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, threshold_apic);
	return 0;
}

SEC("tp_btf/x86_platform_ipi_entry")
int handle__x86_platform_ipi_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

const char x86_platform_ipi[16] = "platform_ipi";
SEC("tp_btf/x86_platform_ipi_exit")
int handle__x86_platform_ipi_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, x86_platform_ipi);
	return 0;
}

SEC("tp_btf/error_apic_entry")
int handle__error_apic_entry(u64 *ctx)
{
	handle_irq_entry();
	return 0;
}

const char error_apic[16] = "error_apic";
SEC("tp_btf/error_apic_exit")
int handle__error_apic_exit(u64 *ctx)
{
	int vector = ctx[0];
	handle_irq_exit(ctx, vector, error_apic);
	return 0;
}

/**
 * handle__softirq_entry - Take a note of softirq starting
 */
SEC("tp_btf/softirq_entry")
int handle__softirq_entry(u64 *ctx)
{
	struct osnoise_variables *osn_var = get_this_cpu_osn_vars();

	if (!osn_var)
		return 0;

	if (!osn_var->sampling)
		return 0;

	osn_var->softirq.count++;
	osn_var->softirq.arrival_time = time_get();
	osn_var->int_counter++;

	return 0;
}

/**
 * handle__softirq_exit - Take a note of softirq leaving
 */
const char sirq_name[16] = "SIRQ";
SEC("tp_btf/softirq_exit")
int handle__softirq_exit(u64 *ctx)
{
	struct osnoise_variables *osn_var = get_this_cpu_osn_vars();
	unsigned int vec_nr = ctx[0];
	s64 delta;

	if (!osn_var)
		return 0;

	if (!osn_var->sampling)
		return 0;

	delta = time_get() - osn_var->softirq.arrival_time;

	post_event(ctx, EVENT_OSNOISE_SOFTIRQ, sirq_name, vec_nr,
		   osn_var->softirq.arrival_time, delta);

	return 0;
}

/**
 * thread_entry - Take a note of a thread getting the processor
 */
static void thread_entry(struct osnoise_variables *osn_var,
			 struct task_struct *p)
{
	if (!osn_var)
		return;

	if (!osn_var->sampling)
		return;

	osn_var->thread.arrival_time = time_get();

	osn_var->int_counter++;
	osn_var->thread.count++;

	return;
}

/**
 * thread_exit - Take a note of a thread leaving the processor
 */
static void thread_exit(u64 *ctx, struct osnoise_variables *osn_var,
			struct task_struct *p)
{
	s64 delta;

	if (!osn_var)
		return;

	if (!osn_var->sampling)
		return;

	if (!osn_var->thread.arrival_time)
		return;

	delta = time_get() - osn_var->thread.arrival_time;

	post_event(ctx, EVENT_OSNOISE_THREAD, p->comm, p->pid,
		   osn_var->thread.arrival_time, delta);

}

/**
 * handle__sched_switch - hook to the sched_switch event
 */
SEC("tp_btf/sched_switch")
int handle__sched_switch(u64 *ctx)
{
	struct osnoise_variables *osn_var = get_this_cpu_osn_vars();
	bool preempt = (bool) ctx[0];
	struct task_struct *prev = (void *) ctx[1];
	struct task_struct *next = (void *) ctx[2];

	if (!osn_var)
		return 0;

	if (prev->pid && (prev->pid != osn_var->pid))
		thread_exit(ctx, osn_var, prev);

	if (prev->pid == osn_var->pid)
		post_switch_event(ctx, false, 0, prev->state);

	if (next->pid && (next->pid != osn_var->pid))
		thread_entry(osn_var, next);
	
	if (next->pid == osn_var->pid)
		post_switch_event(ctx, true, preempt, next->state);

	return 0;
}

char LICENSE[] SEC("license") = "GPL";
