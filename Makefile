INSTALL =       install
BINDIR  :=      /usr/bin

all:
	make -C src
install:
	$(INSTALL) src/osnoise -m 755 $(DESTDIR)$(BINDIR)

clean:
	rm -rf src/osnoise src/.output
